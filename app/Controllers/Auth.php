<?php

namespace App\Controllers;

class Auth extends BaseController
{
    //parte de login e logout

    public function login()
    {

        $email = $this->request->getPost('email');
        $senha = $this->request->getPost('senha');

        $pessoaModel = new \App\Models\pessoa_model();

        $query = $pessoaModel->get();
        foreach ($query->getResult() as $row) {
            if ($row->email == $email) {
                if (password_verify($senha, $row->senha)) {
                    $db = \Config\Database::connect();

                    $query   = $db->query("SELECT nome, email FROM usuarios WHERE email = '$email' LIMIT 1");
                    $row   = $query->getRowArray();



                    date_default_timezone_set('America/fortaleza');
                    $dados = [
                        'email_user' => $email,
                        'ip_user' => getHostByName(php_uname('n')),
                        'data_login' => date('Y/m/d'),
                        'horario_login' =>  date('H:i:s'),
                        'tipo_evento' => 'LOGIN'
                    ];
                    $update_usuarios = [
                        'ip' => getHostByName(php_uname('n')),
                        'data_login' => date('Y/m/d'),
                        'horario_login' =>  date('H:i:s')
                    ];

                    $usuarioModel = new \App\Models\eventos_login_model();
                    $usuarioModel->insert($dados);

                    $db      = \Config\Database::connect();
                    $update_usuarioModel = $db->table('usuarios');

                    $update_usuarioModel->where('email', $email);
                    $update_usuarioModel->update($update_usuarios);

                    $this->session->set('logado', 1);
                    //$this->session->set('id', $row['id']);
                    $this->session->set('email', $row['email']);
                    $this->session->set('nome', $row['nome']);
                    return redirect()->to(base_url('user/index_login'));
                }
            }
        }


        $this->session->setFlashData('msgErro', 'Email ou Senha invalidos.');
        return redirect()->to(base_url('/'));
    }

    public function logout()
    {
        $this->session->destroy();

        return redirect()->to(base_url('/'));
    }

}
